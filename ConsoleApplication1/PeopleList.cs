﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    public class PeopleList
    {
        List<People> nameList;

        public PeopleList()
        {
            this.nameList = new List<People>();
        }

        /// <summary>
        /// The function is for reading data from the text file
        /// </summary>
        /// <param name="fileName">The file name with the absolute/relative path</param>
        public void readList(string fileName)
        {
            // Check file
            if (fileName == "" || fileName == null) throw new Exception("File is not given");

            // Read file
            string[] lines;
            try
            {
                lines = System.IO.File.ReadAllLines(fileName);
            }
            catch (System.IO.FileNotFoundException)
            {
                throw new Exception("Cannot find the file");
            }

            // empty the list
            this.nameList.Clear();
            
            // put each row with a name and a score to the list
            foreach (string line in lines)
            {
                string[] people = line.Split((','));

                // Check format and validity
                if (people.Length < 3)
                    throw new Exception("Missing some attributes for each people");
                else if (people.Length > 3)
                    throw new Exception("Too many attributes for each people");
                else
                {
                    if (!people[0].All(Char.IsLetter) && !people[1].All(Char.IsLetter))
                        throw new Exception("Name only can contain alphabet");
                    int score;
                    if (!int.TryParse(people[2], out score) || score < 0)
                        throw new Exception("Score should be numerical and positive");
                    else

                        // Add the data to the list 
                        this.nameList.Add(new People(people[0], people[1], int.Parse(people[2])));
                }

            }  

        }

        /// <summary>
        /// The function is for ordering the name list by score, surname then given name
        /// </summary>
        public void orderByScore()
        {
            if (!this.nameList.Any())
                throw new Exception("Nobody is contained in the list");
            else
                // Sort the list by score, surname then given name
                this.nameList = this.nameList.OrderByDescending(a => a.getScore()).ThenBy(a => a.getSurname()).ThenBy(a => a.getGivenName()).ToList();
        }

        /// <summary>
        /// The function is for returning the name list
        /// </summary>
        /// <returns>
        /// Return the name list with surname, given name, score
        /// </returns>
        public string getList()
        {
            // Format the list result 
            string output = "";
            foreach (People p in nameList)
            {
                output += String.Format("{0},{1}, {2}\r\n", p.getSurname(), p.getGivenName(), p.getScore());
            }
            return output;
        }

        /// <summary>
        /// The funciton is for print out the name list to text file
        /// </summary>
        /// <param name="fileName">The name of text file which will be written</param>
        public void outputToFile(string fileName)
        {
            // Output the result to a text file
            System.IO.File.WriteAllText(fileName, this.getList());
        }


    }

    class People
    {
        private string surname;
        private string givenName;
        private int score;

        public People(string surname, string givenName, int score)
        {
            this.setSurname(surname);
            this.setGivenName(givenName);
            this.setScore(score);
        }

        public void setSurname(string surname)
        {
            this.surname = surname;
        }

        public string getSurname()
        {
            return this.surname;
        }

        public void setGivenName(string givenName)
        {
            this.givenName = givenName;
        }

        public string getGivenName()
        {
            return this.givenName;
        }

        public void setScore(int score)
        {
            this.score = score;
        }

        public int getScore()
        {
            return this.score;
        }
    }
}
