﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ConsoleApplication1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1.Tests
{
    [TestClass()]
    public class PeopleListTests
    {
        [TestMethod()]
        public void PeopleListInitTest()
        {
            PeopleList pl = new PeopleList();
        }

        [TestMethod()]
        public void readListTest()
        {
            PeopleList pl = new PeopleList();
            pl.readList("names.txt");
            string expected = "BUNDY, TERESSA, 88\r\nSMITH, ALLAN, 70\r\nKING, MADISON, 88\r\nSMITH, FRANCIS, 85\r\n";
            Assert.AreEqual(expected, pl.getList());
        }

        [TestMethod()]
        [ExpectedException(typeof(Exception),
            "Cannot find the file")]
        public void readListFileNotExistTest()
        {
            PeopleList pl = new PeopleList();
            pl.readList("name.txt");
        }

        [TestMethod()]
        [ExpectedException(typeof(Exception),
            "File is not given")]
        public void readListNoFileGivenTest()
        {
            PeopleList pl = new PeopleList();
            pl.readList("");
        }

        [TestMethod()]
        [ExpectedException(typeof(Exception),
            "Missing some attributes for each people")]
        public void readListMissingScoreTest()
        {
            PeopleList pl = new PeopleList();
            pl.readList("names_missing_score_test.txt");
        }

        [TestMethod()]
        [ExpectedException(typeof(Exception),
            "Score should be numerical and positive")]
        public void readListWrongFormatScoreTest()
        {
            PeopleList pl = new PeopleList();
            pl.readList("names_score_isNotNumber.txt");
        }

        [TestMethod()]
        [ExpectedException(typeof(Exception),
            "Name only can contain alphabet")]
        public void readListWrongFormatNameTest()
        {
            PeopleList pl = new PeopleList();
            pl.readList("names_name_wrong_format.txt");
        }

        [TestMethod()]
        [ExpectedException(typeof(Exception),
            "Name only can contain alphabet")]
        public void readListTooManyAttributesTest()
        {
            PeopleList pl = new PeopleList();
            pl.readList("names_too_many_attributes.txt");
        }

        [TestMethod()]
        [ExpectedException(typeof(Exception),
            "Nobody is contained in the list")]
        public void orderByScoreNobodyInTheListTest()
        {
            PeopleList pl = new PeopleList();
            //pl.readList("names.txt");
            pl.orderByScore();
        }

        [TestMethod()]    
        public void orderByScoreTest()
        {
            PeopleList pl = new PeopleList();
            pl.readList("names.txt");
            pl.orderByScore();

            string expected = "BUNDY, TERESSA, 88\r\nKING, MADISON, 88\r\nSMITH, FRANCIS, 85\r\nSMITH, ALLAN, 70\r\n";
            Assert.AreEqual(expected, pl.getList());
        }

        [TestMethod()]
        public void outputToFileTest()
        {
            PeopleList pl = new PeopleList();
            pl.readList("names.txt");
            pl.orderByScore();
            pl.outputToFile("names-graded.txt");
            pl.readList("names-graded.txt");

            string expected = "BUNDY, TERESSA, 88\r\nKING, MADISON, 88\r\nSMITH, FRANCIS, 85\r\nSMITH, ALLAN, 70\r\n";
            Assert.AreEqual(expected, pl.getList());
        }

    }
}